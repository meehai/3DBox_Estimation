# This only gives access to the 2D box system which can input an image and output a classified image.

# SqueezeDet
from .squeezeDet.loader import get2DBox, loadModel, updateImage
